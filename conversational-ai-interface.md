# Conversational interface prototype

AI: I’m glad to see you back. What’s up? 

DE: I’ve got a trip coming up to Montreal. 

AI: From home? 

DE: Yeah. 

AI: Ah, OK. So, that’s about 5 or 6 hours no matter what. Are you going by yourself? 

DE: Yes, I’m going to visit my old roommate, she and her band have a show. 

AI: Great! So, that’s a lot of driving for one weekend, if you go by car. 

DE: I know, and I can’t leave till Friday after work, so it’s going to be rough getting out of town. 

AI: OK, but do you think you could make it to the train station downtown by 5PM? 

DE: It’s right by my office. 

AI: All right. Well, that’s going to be about $250 round trip. But you’d pay about $75 each way just in gas, and you’d also have to stop to fill up. 

DE: I usually have to stop 3 or 4 times on that trip anyway! Plus getting some food, too. 

AI: Right. If you take the train, you can eat in the dining car. So you don’t lose any time. 

DE: What about taking a plane? 

AI: Well, when are you leaving? 

DE: Next Friday. 

AI: Oof. OK, that’s going to be like $300 each way. And you have to get over to Billy Bishop, plus you have to get in to town from the Montreal airport, which is another $50 or so. 

DE: Maybe Jane could pick me up. 

AI: Is that your roommate? 

DE: Ex-roommate, yeah. 

AI: OK, so, it’s still pretty expensive. 

DE: Isn’t it faster though? 

AI: No, about the same time door-to-door with train and plane. 

DE: So, the train sounds like the right choice. 

AI: Well, it’s a little more expensive than driving, but it’ll be much lower stress, a shorter total trip since you don’t have to stop, and it’s way better for the environment. 

DE: Way better? 

AI: It’s like 3 times smaller carbon footprint. Ten times smaller than the plane. 

DE: OK, so, can you get me tickets and send them to my phone? 

AI: On it! Do you want to pay the carbon offset? 

DE: How much is it? 

AI: Just a few dollars. 

DE: Ha! Totally. 

AI: OK, so, we lucked out and got a discount on the return ticket; it’s only $75, so we’re close to the price of a car trip after all. I put the tickets in your wallet. What are you going to do on the train? 

DE: Oh, I don’t know. I have some work to catch up on, but it’s mostly reading and commenting.  

AI: You should bring your iPad; you can watch a movie after you finish your work. That way you don’t have to lug your laptop around all weekend. 

DE: That’d be nice. 

AI: Also, you know there’s a good Irish pub just a couple of blocks from the train station. The Vieux Dublin. You’ll be getting in before midnight. 

DE: Oh right! I’ve been there. They have music, too. Can you tell my roommate to meet me there Friday night? I’ll be ready for a drink. 

AI: What about the show? 

DE: Oh, that’s not till Saturday night. 

AI: Oh, great. All right, it’s done. It’ll be a good way to start the weekend. 

DE: Yeah! 
# Birthday 2023

These are suggested gifts for my Birthday presents for 2023.

- Palestine, a 4000 year history (book)
- Flore laurentienne (book)
- A fishing rod
- A tackle box
- Earrings
- A nose stud
- Bay rum
- Viticulture (board game)
- Long skateboard, like [Retrospec Zed Longboard](https://www.amazon.ca/dp/B08XN12BLN/)
- A [Dymaxion map](https://www.bfi.org/product/dymaxion-map-archival-matte-paper-poster/)
- An amber beer-making kit from [Choppe à Barrock](https://maps.app.goo.gl/qXgErZ3iiT9QuhNb7)
- A mosaic-making kit, like [this one of the Solomon's Knot from INTERESSE](https://www.etsy.com/ca/listing/877475860/mosaic-kit-solomons-knot-2-tutorial).
- A bench with rack for a barbell, like [this one from Soozier](https://www.bestbuy.ca/en-ca/product/soozier-adjustable-weight-bench-folding-lifting-flat-black/12656653)
- A sundial for the country house
- Bookshelves for the wall in my office of the country house
- Parts to make a [water clock](https://www.wikihow.com/Make-a-Water-Clock-%28Clepsydra%29)
- Rain barrel for country house
- pot and cups for [cafe de olla](https://en.wikipedia.org/wiki/Caf%C3%A9_de_olla)
- Drawstring pants from Uniqlo (size XL, 36 waist)
- Sweatpant shorts (size XL, 36 waist)
- XL t-shirts
- XL button-down shirts
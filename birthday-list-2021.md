# Birthday list 2021

These are suggested gifts for my birthday for 2021.

- A light fleece, for wearing inside or while running, waterproof is nice
- A compact weightlifting tool for Montreal, like [Gorilla Bow](https://gorillabow.ca/products/gorilla-bow)
- A bench with rack for a barbell, like [this one from Soozer](https://www.bestbuy.ca/en-ca/product/soozier-adjustable-weight-bench-folding-lifting-flat-black/12656653)
- An electric chainsaw for trimming bushes and trees, like [this one from Craftsman](https://www.rona.ca/en/craftsman-electric-chainsaw-8-a-14-in-red-cmecs614-00277237)
- New true wireless bluetooth headphones, like the [Sony WF-1000XM4](https://www.sony.ca/en/electronics/truly-wireless/wf-1000xm4)
- A waffle iron for the country house
- A sundial for the country house
- Grapes for making wine
- A mesh bag for making wine
- A big plastic tub for making wine
- Canning tongs for the country house
- Pictures of things in Jerusalem for my office in the country house
- Bookshelves for the wall in my office of the country house
- Sailing lessons
- A tandem trip on an ultralight plane, like [this one in St. Hyacinthe](https://www.volrecreatif.com/ultraleger_pendulaire_tour.php)
- Stuff to make a [water clock](https://www.wikihow.com/Make-a-Water-Clock-%28Clepsydra%29)
- A bike ride together
- toe clips for my bike, like [EVO clipless](https://www.sportsexperts.ca/en-CA/p-strapless-bike-toe-clips/385756/385756-2)


# Gift list

This is my running wishlist of gifts I'd like to get.

- A shearling coat, XXL, doesn't have to be real leather or wool
- Big and small swing-top bottles
- Champagne yeast, campden tablets, and other wine-making supplies
- A design for my wine bottle labels (with space for the name of different wines)
- A wine corker
- A wine press
- An indoor greenhouse like [this one from Vivosun](https://www.amazon.ca/dp/B07JZ7JQ47/)
- A fishing rod
- A tackle box
- A nose stud
- Viticulture (board game)
- Long skateboard, like [Retrospec Zed Longboard](https://www.amazon.ca/dp/B08XN12BLN/)
- A [Dymaxion map](https://www.bfi.org/product/dymaxion-map-archival-matte-paper-poster/)
- A mosaic-making kit, like [this one of the Solomon's Knot from INTERESSE](https://www.etsy.com/ca/listing/877475860/mosaic-kit-solomons-knot-2-tutorial).
- A bench with rack for a barbell, like [this one from Soozier](https://www.bestbuy.ca/en-ca/product/soozier-adjustable-weight-bench-folding-lifting-flat-black/12656653)
- A sundial for the country house
- Bookshelves for the wall in my office of the country house
- Parts to make a [water clock](https://www.wikihow.com/Make-a-Water-Clock-%28Clepsydra%29)
- Rain barrel for country house
- pot and cups for [cafe de olla](https://en.wikipedia.org/wiki/Caf%C3%A9_de_olla)
- Sweatpant shorts (size XXL, 38 waist)
- XXL t-shirts
- XXL button-down shirts
- Big jugs for wine-making
OK, so, let's game this out!

1. Canadian member-run co-op 

2. 1 co-op membership gets you 1 fediverse account with fixed allowances (uploads, API use, toots per day, etc.) plus a co-op vote

3. has local services per neighbourhood or city, but membership is national

4. run a first emergency server nationally to catch the next wave of Twitter migration

5. Member run, nobody gets paid for now

6. CAD$60/year membership fee

7. I just registered cosocial.ca so let's use that as a name for now

If this sounds good, we can get the papers filed today and start setting up a server this weekend.

We need:

- 4 other people to be directors/incorporators. Diverse voices, including women and nonbinary, 2SLGTBQIA+, Black, Indigenous, fr/en, needed up front.

- Someone to fill out the paperwork

- Someone to collect and manage $$

- Someone(s) to set up the server on Canadian k8s service

Shall we?



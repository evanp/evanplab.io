# Father's Day 2022

These are suggested gifts for my Father's Day presents for 2022.

- A mosaic-making kit, like [this one of the Solomon's Knot from INTERESSE](https://www.etsy.com/ca/listing/877475860/mosaic-kit-solomons-knot-2-tutorial).
- An electric lawn mower, like [this one from Ryobi](https://www.homedepot.com/p/RYOBI-ONE-HP-18V-Brushless-16-in-Cordless-Battery-Walk-Behind-Push-Lawn-Mower-with-2-4-0-Ah-Batteries-and-1-Charger-P1190VNM/314600700)
- A bench with rack for a barbell, like [this one from Soozier](https://www.bestbuy.ca/en-ca/product/soozier-adjustable-weight-bench-folding-lifting-flat-black/12656653)
- A Raspberry Pi 4 with 8Gb memory, like [this one from Canakit](https://www.canakit.com/raspberry-pi-4-starter-kit.html)
- A waffle iron for the country house
- A sundial for the country house
- Canning tongs for the country house
- A set of beer mugs, like [these from Chefcaptain](https://www.amazon.ca/dp/B00V5F2G5G/)
- Bookshelves for the wall in my office of the country house
- Sailing lessons
- A tandem trip on an ultralight plane, like [this one in St. Hyacinthe](https://www.volrecreatif.com/ultraleger_pendulaire_tour.php)
- Parts to make a [water clock](https://www.wikihow.com/Make-a-Water-Clock-%28Clepsydra%29)

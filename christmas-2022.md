# Christmas 2022

These are suggested gifts for my Birthday presents for 2022.

- A mosaic-making kit, like [this one of the Solomon's Knot from INTERESSE](https://www.etsy.com/ca/listing/877475860/mosaic-kit-solomons-knot-2-tutorial).
- A bench with rack for a barbell, like [this one from Soozier](https://www.bestbuy.ca/en-ca/product/soozier-adjustable-weight-bench-folding-lifting-flat-black/12656653)
- A Raspberry Pi 4 with 8Gb memory, like [this one from Canakit](https://www.canakit.com/raspberry-pi-4-starter-kit.html)
- A sundial for the country house
- A set of beer mugs, like [these from Chefcaptain](https://www.amazon.ca/dp/B00V5F2G5G/)
- Bookshelves for the wall in my office of the country house
- Sailing lessons
- Parts to make a [water clock](https://www.wikihow.com/Make-a-Water-Clock-%28Clepsydra%29)
- A flat cloth cap, like the one I used to have
- Earrings, especially silver and black
- Lawn-aerating sandals like https://www.amazon.ca/Yerdos-Assembled-Aerating-Soleplate-Stainless/dp/B09Z3SVCDS/
- Rain barrel for country house
- Brie-roasting pan
- Bat house like https://www.amazon.ca/Kenley-Bat-House-Handcrafted-Resistant/dp/B078HPMN7B/
- pot and cups for [cafe de olla](https://en.wikipedia.org/wiki/Caf%C3%A9_de_olla)
